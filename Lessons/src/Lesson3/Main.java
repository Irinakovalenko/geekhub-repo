package Lesson3;

public class Main {
    public static void main(String[] args) {
        testStr();
        testStringBuilder();
        testStringBuffer();
    }

    private static void testStr() {
        long startTime = 0, endTime;
        String s = new String("(O_O)");
        System.out.println("String:");
        for(int i=0; i< 10000; i++) {
            String p = "s";
            s += p ;
            if (i == 0) {
                startTime = System.currentTimeMillis();
                System.out.println(" Start time - " + startTime);
            }
        }
        endTime = System.currentTimeMillis();
        System.out.println(" End time -   " + endTime);
        System.out.println(" Difference - " + (endTime - startTime));

    }

    private static void testStringBuffer() {
        long startTime = 0, endTime;
        StringBuffer stringBuffer = new StringBuffer();
        System.out.println("StingBuffer:");
        for (int i = 0; i < 10000; i++) {
            String p = ":)";
            p += p ;
            if (i == 0) {
                startTime = System.currentTimeMillis();
                System.out.println(" Start time - " + startTime);
            }

            stringBuffer.append(p);
        }
        endTime = System.currentTimeMillis();
        System.out.println(" End time -   " + endTime);
        System.out.println(" Difference - " + (endTime - startTime));

    }

    private static void testStringBuilder() {
        long startTime = 0, endTime;
        StringBuilder stringBuilder = new StringBuilder();
        System.out.println("StingBuilder:");
        for (int i = 0; i < 10000; i++) {   // к надписи  BBC news добавляем 10 000 раз BBC news
            String p = " :D";
            p += p ;
            if (i == 0) {
                startTime = System.currentTimeMillis();
                System.out.println(" Start time - " + startTime);
            }
            stringBuilder.append(p);
        }
        endTime = System.currentTimeMillis();
        System.out.println(" End time -   " + endTime);
        System.out.println(" Difference - " + (endTime - startTime));

    }

}
