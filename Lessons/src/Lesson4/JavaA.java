package Lesson4;

import java.util.Set;
import java.util.HashSet;
import java.util.Random;

interface SetOperations {

    public boolean equals(Set a, Set b);

    public Set union(Set a, Set b);

    public Set subtract(Set a, Set b);

    public Set intersect(Set a, Set b);

    public Set symmetricSubtract(Set a, Set b);
}
class NewSetOperations implements SetOperations {

    @Override
    public boolean equals(Set a, Set b) {
        return a.equals((Set) b) ? true : false;
    }

    @Override
    public Set union(Set a, Set b) {
        Set c = new HashSet(a);
        c.addAll(b);
        return c;
    }

    @Override
    public Set subtract(Set a, Set b) {
        Set c = new HashSet(a);
        c.removeAll(b);
        return c;
    }

    @Override
    public Set intersect(Set a, Set b) {
        Set c = new HashSet(a);
        c.retainAll(b);
        return c;
    }

    @Override
    public Set symmetricSubtract(Set a, Set b) {
        return union(subtract(a, b), subtract(b, a));
    }
}
public class JavaA {
    NewSetOperations operations = new NewSetOperations();

    public static void main(String[] args) {
        Set a = new HashSet();
        for(int i = 0; i < 10; ++i){
            a.add((int) (Math.random() * 15));
        }
        System.out.println(" А:");
        System.out.println(a);

        Set b = new HashSet();
        for(int i = 0; i < 8; ++i){
            b.add((int) (Math.random() * 16));
        }
        System.out.println(" В:");
        System.out.println(b);

        NewSetOperations test = new NewSetOperations();
        if(test.equals(a, b)){
            System.out.println(" (А=В)");
        } else {
            System.out.println("Mножества А и В не равны");
        }
        System.out.println(" (A ∪ B):");
        System.out.println(test.union(a, b));

        System.out.println("Разность множеств А и В (AB):");
        System.out.println(test.subtract(a, b));

        System.out.println(" (А ∩ В):");
        System.out.println(test.intersect(a, b));

        System.out.println(" (А Δ В):");
        System.out.println(test.symmetricSubtract(a, b));

        System.out.println("end");


    }
}