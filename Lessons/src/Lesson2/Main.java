package Lesson2;
import java.io.IOException;
public class Main {
    public static  void main(String [] args) throws IOException  {
        Vehicle transport;

        System.out.println("Choose transport:");
        System.out.println("1 - Gas car");
        System.out.println("2 - Solar-powered car");
        System.out.println("3 - Boat");

        int choice = System.in.read();

        switch (choice) {
            case '1':
                transport = new GasCar();
                break;
            case '2':
                transport = new SolarCar();
                break;
            case '3':
                transport = new Boat();
                break;
            default:
                transport = new GasCar();
                break;
    }
        System.out.println("Controls:");
        System.out.println("A - accelerate");
        System.out.println("B - brake");
        System.out.println("T - turn ");
        System.out.println("Q - quit");

        while (true) {
            choice = System.in.read();

            if (choice == 'q') {
                break;
            }
            switch (choice) {
                case 'A':
                    transport.accelerate();
                    break;
                case 'B':
                    transport.brake();
                    break;
                case 'T':
                    transport.turn();
            }
        }
    }
}
