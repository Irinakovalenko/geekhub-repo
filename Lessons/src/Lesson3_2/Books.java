package Lesson3_2;

 class Book implements Comparable <Book>{

    private String authorAndTitle;
    private String type;
    private int page;

    public Book(String authorAndTitle, String type, int page) {
        this.authorAndTitle = authorAndTitle;
        this.type = type;
        this.page = page;
    }

    @Override
    public int compareTo(Book o) {
        if (!this.type.equals(o.type)) {
            return this.type.compareTo(o.type);
        } else {
            if (this.page != o.page) {
                if (this.page < o.page) {
                    return -1;
                } else {
                    return 1;
                }
            }
            return 0;
        }
    }
    public void output() {
        System.out.printf("%s\t%s\t%d\n", type, authorAndTitle,page);
    }
}
