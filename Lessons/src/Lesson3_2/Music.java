package Lesson3_2;


 public class Music implements Comparable <Music> {
     private String groupName;
     private String songTitle;

     public Music(String groupName, String songTitle) {
         this.groupName =groupName;
         this.songTitle = songTitle;
     }

     @Override
     public int compareTo(Music o) {
         return this.groupName.compareTo(o.groupName);
     }

     public void output() {
         System.out.printf("%s %s\n", groupName, songTitle);
     }
 }

