package Less2;
import java.util.Scanner;

public class SolarCar extends Vehicle {

    ElectricEngine electcEngine = new ElectricEngine();
    private static Scanner input=new Scanner(System.in);


    public void accelerate() {

        int speed=input.nextInt();
        electcEngine.addSpeed(speed);
        electcEngine.payment();
    }

    public void turn (String direction) {
        super.turn(direction);
        electcEngine.payment();
    }


    public void brake() {
        int speed=input.nextInt();
        electcEngine.differenceSpeed(speed);
        electcEngine.payment();
    }

    SolarCar(){
        electcEngine.speedNow();
    }
}
