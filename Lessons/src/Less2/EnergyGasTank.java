package Less2;
import java.util.Scanner;

public class EnergyGasTank {

    private int maxCapacity=100;
    private double fuel;

    public double getFuel() {
        return fuel;
    }

    public void setFuel(double fuel) {
        this.fuel = fuel;
    }

    public double inputGas (){
        System.out.print("Количество бензина в баке  " );
        Scanner inpP=new Scanner(System.in) ;
        fuel= inpP.nextInt();
        if (fuel> maxCapacity)
            fuel =  maxCapacity;
        System.out.println( "  " +fuel );

        return  fuel ;
    }

}

