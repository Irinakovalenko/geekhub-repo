package com.geekhub.lessons;

import java.util.Scanner;
public class LessonsOne {
public static void main (String[] args){
    Scanner in = new Scanner(System.in);
     int n = in.nextInt();
     int fact = 1;
     for(int i = n; i >= 1;i--) {
         fact*=i;
     }
    System.out.println("Factorial! = " + fact) ;
 }
}
